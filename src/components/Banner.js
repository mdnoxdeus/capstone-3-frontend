import {Col} from 'react-bootstrap';

export default function Banner({bannerData}) {
	return(
		<div className="p-5 text-center">
			<Col>
				<h1>{bannerData.title}</h1>
			</Col>
		</div>
	);
};
