import {Card, Container, Image} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function UpdateCard({updateProp}) {
    return(
    <Container>
      <Card.Body>
        <Image src={updateProp.image} width="30%" alt=""/>
        <Card.Title>
          {updateProp.name}
        </Card.Title>
        <Card.Text>
          Price: {updateProp.price}
        </Card.Text>
        <Link to={`update/${updateProp._id}`} className="btn btn-primary">
          Update Product
        </Link>
      </Card.Body>
    </Container>
  )
}