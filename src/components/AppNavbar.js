import {useContext} from 'react';
import {Navbar, Nav} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import UserContext from '../UserContext';

function AppNavbar() {
	const {user} = useContext(UserContext);
	return(
		<Navbar collapseOnSelect expand="lg" variant="dark" fixed="top">
			<a href="</">
				<img alt="" src="/mdnox64.ico" width="30" height="30" />{' '}
			</a>
			<Navbar.Brand className="navbarBrand mx-2" href="/">M D N O X D E U S</Navbar.Brand>


			<Navbar.Toggle aria-controls="responsive-navbar-nav"/>
			<Navbar.Collapse id="responsive-navbar-nav">
			    <Nav className="mr-auto">
				    {
				    	user.isAdmin === true ? <><Link className="nav-link" to="/admin">ADMIN</Link><Link className="nav-link" to="/products">SHOP</Link></>
				    	: <Link className="nav-link" to="/products">SHOP</Link>
				    }
			    </Nav>

			    <Nav className="ml-auto">
			    	{
			    		user.id ? <Link className="nav-link" to="/logout">LOGOUT</Link>
			    		: <Link className="nav-link" to="/signin">SIGN IN</Link>
			    	}
			    </Nav>
			</Navbar.Collapse>
		</Navbar>
	);
};

export default AppNavbar;