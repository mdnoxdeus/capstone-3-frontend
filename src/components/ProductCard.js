import {Card, Image} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function ProductCard({productProp}) {
  return(
    <Card className="cardForm m-2 d-md-inline-flex">
      <Card.Body>
        <Image width="100%" src={productProp.image}alt=""/>
        <Card.Title>
          {productProp.name}
        </Card.Title>
        <Card.Text>
          Price: {productProp.price}
        </Card.Text>
        <Link to={`viewproduct/${productProp._id}`} className="btn btn-primary">
          View Product
        </Link>
      </Card.Body>
    </Card>
  )
}