import {UserProvider} from './UserContext.js';
import {useState, useEffect} from 'react';
import Navbar from './components/AppNavbar';
import Home from './pages/Home';
import Shop from './pages/Products';
import View from './pages/ViewProduct';
import SignIn from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Admin from './pages/AdminControl';
import Add from './pages/AddProduct';
import All from './pages/AllProducts';
import Update from './pages/UpdateProduct';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import './App.css';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
    setUser({
      id: null,
      isAdmin: null
    });
  };

  useEffect(() => { 
    let token = localStorage.getItem('accessToken');
    fetch('https://capstone3-mdnoxdeus.herokuapp.com/account/user', {
      headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(res => res.json())
      .then(convertedData => {
      if (typeof convertedData._id !== "undefined") {
        setUser({
          id: convertedData._id, 
          isAdmin: convertedData.isAdmin
        });
      } else {
        setUser({
          id: null, 
          isAdmin: null
      });
    }
  });
  },[user]);

  return(
    <UserProvider value={{user, setUser, unsetUser}}> 
      <div className="bgbody">
        <Router>
        <Navbar/>
          <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path="/products" element={<Shop/>}/>
            <Route path="/products/viewproduct/:productId" element={<View/>}/>
            <Route path="/admin" element={<Admin/>}/>
            <Route path="/admin/addproduct" element={<Add/>}/>
            <Route path="/admin/allproducts" element={<All/>}/>
            <Route path="/admin/allproducts/update/:productId" element={<Update/>}/>
            <Route path="/signin" element={<SignIn/>}/>
            <Route path="/logout" element={<Logout/>}/>
            <Route path="/register" element={<Register/>}/>
          </Routes>
        </Router>
      </div>
    </UserProvider>
  );
};

export default App;