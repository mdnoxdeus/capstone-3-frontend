import {useState, useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom'
import {Form, Button, Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';
import Banner from './../components/Banner';
import Swal from 'sweetalert2';

const data = {
	title: 'SIGN IN',
};

export default function SignIn() {

	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	let addressSign = email.search('@');
	let dns = email.search('.com')
	const [isActive, setIsActive] = useState(false);
	const [isValid, setIsValid] = useState(false);

	useEffect(() => {
		if (dns !== -1 && addressSign !== -1) {
			setIsValid(true);
			if (password !== '') {
				setIsActive(true)
			} else {
				setIsActive(false);
			}
		} else {
			setIsValid(false);
			setIsActive(false);
		};
	},[email, password, addressSign, dns]);

	const loginUser = async (event) => {
		event.preventDefault();
		fetch('https://capstone3-mdnoxdeus.herokuapp.com/account/login' , {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}).then(res => res.json())
		.then(jsondata => {
			let token = jsondata.accessToken;
			if (typeof token !== 'undefined') {
				localStorage.setItem('accessToken', token);
				fetch('https://capstone3-mdnoxdeus.herokuapp.com/account/user', {
					headers: {
						Authorization: `Bearer ${token}`
					}
				})
				.then(res => res.json())
				.then(convertedData => {
					if (typeof convertedData._id !== "undefined") {
						setUser({
							id: convertedData._id,
							isAdmin: convertedData.isAdmin
						});
						Swal.fire({
							icon: 'success',
							title: 'Logged in successfully.'
						});
					} else {
						setUser({
						    id: null, 
						    isAdmin: null
						});
					}
				});
			} else {
				Swal.fire({
					icon: 'error',
					title: 'Login failed.',
					text: 'Check credentials.'
				});
			}
		})
	};

	return(
		user.id ? <Navigate to="/" replace={true}/>
		:
		<div>
			<Banner bannerData={data}/>
			<Container>
				<Form onSubmit={e => loginUser(e)}>
					<Form.Group>
						<Form.Label>Email:</Form.Label>
						<Form.Control
							type="email"
							placeholder="Input email here"
							required
							value={email}
							onChange={e => {setEmail(e.target.value)}}
						/>
						{
							isValid ? <h6 className="text-success"> Email is Valid.</h6>
							: <h6 className="text-mute"> Email is invalid.</h6>
						}
					</Form.Group>
					<Form.Group>
						<Form.Label>Password:</Form.Label>
						<Form.Control
							type="password"
							placeholder="Input password here"
							required
							value={password}
							onChange={e => {setPassword(e.target.value)}}
						/>
					</Form.Group>
					{
						isActive ? <Button variant="secondary" className="btn-block" type="submit">Login</Button>
						: <Button className="btn-block" variant="secondary" disabled>Login</Button>
					}
					
					<h6>New Customer?</h6>
					<Link className="btn btn-block btn-secondary" to="/register">Create Account</Link>
				</Form>
			</Container>
		</div>
	);
};