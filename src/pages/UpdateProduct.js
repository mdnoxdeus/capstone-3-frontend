import {useState, useEffect} from 'react'
import {Form, Button, Container} from 'react-bootstrap'
import {useParams} from 'react-router-dom';
import Swal from 'sweetalert2';
import Banner from	'./../components/Banner';

const details = {
	title: 'UPDATE',
}

export default function ProductUpdate() {
	let token = localStorage.getItem('accessToken');

	const [setProductInfo] = useState({
		name: null,
		price: null,
		image: null
	});

	const {productId} = useParams()
	useEffect(() => {
		fetch(`https://capstone3-mdnoxdeus.herokuapp.com/admin/${productId}`)
		.then(res => res.json())
		.then(convertedData => {
			setProductInfo({
				name: convertedData.name,
				price: convertedData.price,
				image: convertedData.image,
				isActive: convertedData.isActive
			});
		});
	},[productId, setProductInfo])

	const [productName, setProductName] = useState('');
	const [productPrice, setProductPrice] = useState('');
	const [productImage, setProductImage] = useState('');

	const [isUpdated, setIsUpdated] = useState(false);
	useEffect(() => {
		if (productName !== '' && productPrice !== '' && productImage !== '') {
			setIsUpdated(true);
		} else {
			setIsUpdated(false);
		};
	},[productName, productPrice, productImage]);

/*Update Details*/
	const update = async(eventSubmit) => {
		eventSubmit.preventDefault();
		const isUpdated = await fetch(`https://capstone3-mdnoxdeus.herokuapp.com/admin/${productId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				name: productName,
				price: productPrice,
				image: productImage
			})
		}).then(res => res.json()).then(jsondata => {
			if (jsondata) {
				return true;
			} else {
				return false;
			}
		})

		if (isUpdated) {
			setProductName('');
			setProductPrice('');
			setProductImage('');
			await Swal.fire({
				icon: 'success',
				title: 'Product Updated',
			})
			window.location.href = "/admin";
		} else {
			Swal.fire({
				icon: 'error',
				title: 'Something went wrong.',
				text: 'Try again later.'
			});
		};
	};

/*Delete*/
	const remove = async() => {
		await Swal.fire({
			title: 'Are you sure?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Yes',
			width: '300px'
		}).then((result) => {
			if (result.isConfirmed) {
				fetch(`https://capstone3-mdnoxdeus.herokuapp.com/admin/${productId}/delete`, {
					method: 'DELETE',
					headers: {
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${token}`
					}
				}).then(res => res.json()).then(jsondata => {
					if (jsondata) {
						return true;
					} else {
						return false;
					}
				})	
			}
		})
		await Swal.fire('Product Deleted!')
		window.location.href = "/admin";
	};

/*Active Status*/
/*Archive Product*/
	const archive = async() => {
		const isArchived = await fetch(`https://capstone3-mdnoxdeus.herokuapp.com/admin/${productId}/archive`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			}
		}).then(res => res.json()).then(jsondata => {
			if (jsondata) {
				return true;
			} else {
				return false;
			}
		})

		if (isArchived) {
			await Swal.fire({
				icon: 'success',
				title: 'Product Archived',
			})
			window.location.href = "/admin";
		} else {
			Swal.fire({
				icon: 'error',
				title: 'Something went wrong.',
				text: 'Try again later.'
			});
		};
	};

/*Unarchive Product*/
	const activate = async() => {
		const isActivated = await fetch(`https://capstone3-mdnoxdeus.herokuapp.com/admin/${productId}/setActive`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			}
		}).then(res => res.json()).then(jsondata => {
			if (jsondata) {
				return true;
			} else {
				return false;
			}
		})

		if (isActivated) {
			await Swal.fire({
				icon: 'success',
				title: 'Product Now Active',
			})
			window.location.href = "/admin";
		} else {
			Swal.fire({
				icon: 'error',
				title: 'Something went wrong.',
				text: 'Try again later.'
			});
		};
	};


	return(
		<>
			<Banner className="text-center" bannerData={details}/>
			<Container className="mb-5">
				<Form onSubmit={e => update(e)}>
					<Form.Group>
						<Form.Label>Product Name:</Form.Label>
						<Form.Control 
							type="text"
							required
							value={productName}
							onChange={e => setProductName(e.target.value)}
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Product Price:</Form.Label>
						<Form.Control 
							type="number"
							required
							value={productPrice}
							onChange={e => setProductPrice(e.target.value)}
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Product Image:</Form.Label>
						<Form.Control 
							type="text"
							required
							value={productImage}
							onChange={e => setProductImage(e.target.value)}
						/>
					</Form.Group>
					{
						isUpdated ? <Button className="btn-secondary btn-block" type="submit">Update Product</Button>
						: <Button className="btn-secondary btn-block" disabled>Update Product</Button>
					}
					<Button className="btn-warning btn-block" onClick={archive}>Archive Product</Button>
					<Button className="btn-success btn-block" onClick={activate}>Activate Product</Button>
					<Button className="mt-2 btn-danger btn-block" onClick={remove}>Delete Product
					</Button>
				</Form>
			</Container>
		</>
	);
};