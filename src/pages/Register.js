import {useState, useEffect} from 'react'
import {Container, Form, Button} from 'react-bootstrap';
import Banner from	'./../components/Banner';
import Swal from 'sweetalert2'

const details = {
	title: 'NEW ACCOUNT',
}

export default function Register() {

	const [firstName, setFirstName] = useState('');
	const [middleName, setMiddleName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNumber, setMobileNumber] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState(''); 

	const [isRegistered, setIsRegistered] = useState(false);
	const [isMatched, setIsMatched] = useState(false);
	const [isDigits, setIsDigits] = useState(false);
	const [isAllowed, setIsAllowed] = useState(false);

	useEffect(() => {
		if (mobileNumber.length === 11) {
			setIsDigits(true)
			if (password1 === password2 && password1 !== '' && password2 !== '') {
				setIsMatched(true);
				if (firstName !== '' && lastName !== '' && email !== '') {
					setIsAllowed(true);
					setIsRegistered(true);
				} else {
					setIsAllowed(false);
					setIsRegistered(false);
				}
			}
		} else if (password1 !== '' && password1 === password2) {
			setIsMatched(true);
		} else {
			setIsDigits(false);
			setIsRegistered(false);
			setIsMatched(false);
			setIsAllowed(false);
		};
	},[firstName, middleName, lastName, email, password1, password2, mobileNumber]);

	const registerUser = async(eventSubmit) => {
		eventSubmit.preventDefault()

		const isRegistered = await fetch('https://capstone3-mdnoxdeus.herokuapp.com/account/register', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				middleName: middleName,
				lastName: lastName,
				email: email,
				password: password1,
				mobileNumber: mobileNumber
			})
		}).then(res => res.json()).then(jsondata => {
			if (jsondata.email) {
				return true;
			} else {
				return false;
			}
		})

		if (isRegistered) {
			setFirstName('');
			setMiddleName('');
			setLastName('');
			setEmail('');
			setMobileNumber('');
			setPassword1('');
			setPassword2('');
			await Swal.fire({
				icon: 'success',
				title: 'Registration successful',
				text: 'Thank you for registering.'
			})
			window.location.href = "/signin";
		} else {
			Swal.fire({
				icon: 'error',
				title: 'Something went wrong.',
				text: 'Try again later.'
			});
		};
	};

	return(
		<>
			<Banner className="text-center" bannerData={details}/>
			<Container className="mb-5">
				{
					isAllowed ? <h1 className="text-center text-success">You may now register.</h1>
					: <h1 className="text-center">Account Details</h1>
				}
				<Form onSubmit={e => registerUser(e)}>
					<Form.Group>
						<Form.Label>First Name:</Form.Label>
						<Form.Control 
							type="text" 
							placeholder="Enter your first name"
							required
							value={firstName}
							onChange={e => setFirstName(e.target.value)}
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Middle Name:</Form.Label>
						<Form.Control 
							type="text"
							placeholder="Enter your middle name" 
							required
							value={middleName}
							onChange={e => setMiddleName(e.target.value)}
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Last Name:</Form.Label>
						<Form.Control 
							type="text"
							placeholder="Enter your last name" 
							required
							value={lastName}
							onChange={e => setLastName(e.target.value)}
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Email:</Form.Label>
						<Form.Control 
							type="email"
							placeholder="Insert email here"
							required
							value={email}
							onChange={e => setEmail(e.target.value)}
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Mobile Number:</Form.Label>
						<Form.Control 
							type="number"
							placeholder="Insert your mobile number"
							required
							value={mobileNumber}
							onChange={e => setMobileNumber(e.target.value)}
						/>
						{
							isDigits ? <span className="text-success">Mobile Number is valid.</span>
							: <span className="text-muted">Mobile Number invalid.</span>
						}
					</Form.Group>
					<Form.Group>
						<Form.Label>Password:</Form.Label>
						<Form.Control 
							type="password" 
							placeholder="Enter password here"
							required
							value={password1}
							onChange={e => setPassword1(e.target.value)}
						/>	
					</Form.Group>
					<Form.Group>
						<Form.Label>Confirm Password:</Form.Label>
						<Form.Control 
							type="password" 
							placeholder="Confirm your password"
							required
							value={password2}
							onChange={e => setPassword2(e.target.value)}
						/>
						{
							isMatched ? <span className="text-success">Password match.</span>
							: <span className="text-danger">Password should match.</span>
						}
					</Form.Group>
						{
							isRegistered ? <Button className="btn-secondary btn-block" type="submit">Register</Button>
							: <Button className="btn-secondary btn-block" disabled>Register</Button>
						}
				</Form>
			</Container>
		</>
	);
};
